﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class CameraTarget : MonoBehaviour {

	private Rigidbody rigidBody;
	private float rotateSpeed = 100.0f;
	public Camera theCamera;

	void Start () {
		rigidBody = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate ()
	{
		float h = CrossPlatformInputManager.GetAxis ("Horizontal");
		float v = CrossPlatformInputManager.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (h, 0.0f, v);
		rigidBody.transform.Translate (movement);

		bool left = Input.GetKey (KeyCode.Q);
		bool right = Input.GetKey (KeyCode.E);
		if (left || right) {
			float direction = left ? 1f : -1f;
			rigidBody.transform.Rotate (direction * Vector3.up * Time.deltaTime * rotateSpeed);
		}

		float scroll = Input.mouseScrollDelta.y;
		if (scroll != 0f) {
			scroll *= 2f;
			Vector3 move = new Vector3(0f, -scroll, scroll);
			theCamera.transform.Translate(move, rigidBody.transform);
		}
	}
}
