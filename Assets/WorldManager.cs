﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldManager : MonoBehaviour {

	public GameObject obstacle;
	public GameObject work;

	public Transform worldSpace;

	private List<GameObject> obstacles = new List<GameObject> ();
	private List<GameObject> workList = new List<GameObject> ();

	public void CreateObstacles ()
	{

		foreach (Object o in obstacles) {
			Destroy (o);
		}

		float width = 100f; // worldSpace.localScale.x;
		float length = 100f; // worldSpace.localScale.z;

		for (float x = -width; x < width; x += obstacle.transform.localScale.x + 2) {
			for (float z = -length; z < length; z += obstacle.transform.localScale.z + 2) {
				if (Random.value < 0.3f) {
					if (Random.value < 0.8f) {
						CreateAndAdd(x, z, obstacle, obstacles);
					} else {
						CreateAndAdd(x, z, work, workList);
					}
				}
			}
		}
	}

	public GameObject GetWork ()
	{
		if (workList.Count > 0)
			return workList[Random.Range(0, workList.Count)];
		else
			return null;
	}

	private void CreateAndAdd(float x, float z, GameObject thing, List<GameObject> list)
	{
		Vector3 defaultPosition = thing.transform.position;
		Vector3 position = new Vector3 (x, defaultPosition.y, z);
		GameObject o = Instantiate (thing, position, thing.transform.rotation) as GameObject;
		list.Add (o);
	}

}
