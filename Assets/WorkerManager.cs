﻿using System;
using UnityEngine;
using System.Collections;

public class WorkerManager : MonoBehaviour {

//	private static WorkerManager workerManager;

	
	private Transform goal;
	private WorldManager worldManager;


	private NavMeshAgent agent;

//	public static WorkerManager Instance ()
//	{
//		if (!workerManager) {
//			workerManager = FindObjectOfType(typeof (WorkerManager)) as WorkerManager;
//			if (!workerManager)
//				Debug.LogError("There needs to be one active WorkerManager script on a GameObject in your scene.");
//		}
//		return workerManager;
//	}

    private void Start()
    {
    	worldManager = (WorldManager) FindObjectOfType(typeof(WorldManager));
    }


    private void Update ()
	{
		if (goal) {
			if (Vector3.Distance (transform.position, goal.position) < 1f) {
				goal = null;
			}
		} else {
			GameObject workObject = worldManager.GetWork ();
			if (workObject) {
				goal = workObject.transform;
				agent = GetComponentInChildren<NavMeshAgent> ();
				agent.destination = goal.position;
			}
		}
    }

}
